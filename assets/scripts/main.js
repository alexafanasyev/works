var ENDPOINTS = {
    BASE: '/api/v1',
    WORKS: '/works',
    LOGIN: '/login'
};

jQuery(document).ready(
    function () {
        ajaxSetup();
        attachHandlers();
        var loggedIn = checkLogin();
        if (!loggedIn) {
            showLoginPage();
        } else {
            showWorksPage();
        }
    }
);

function attachHandlers() {
    jQuery('body')
        .on('click', '#login_page .login-button', authenticate)
        .on('click', '.logout-button', logout)
}


function showLoginPage() {
    jQuery('.logout-button').hide();
    jQuery('#login_page').show();
    jQuery('#works_page').hide();
}

function showWorksPage() {
    jQuery('.logout-button').show();
    jQuery('#login_page').hide();
    jQuery('#works_page').show();
    updateWorks();
}

function updateWorks() {
    getWorks().then(
        function (works) {
            jQuery('#works_page .works-list').empty();
            jQuery.each(works, function (index, value) {
                var template = getWorkItem(value.id, value.name, value.score);

                // disable scored items
                if (value.score !== null) {
                    template.find('.btn').attr('disabled', 'disabled')
                }
                jQuery('#works_page .works-list').append(template);
            })
        },
        function (err) {
            showMessage()
        }
    )
}

function rate(elem, value) {
    var id = jQuery(elem).parents('.work').attr('data-id');
    jQuery.ajax({
        method: 'put',
        url: ENDPOINTS.BASE + ENDPOINTS.WORKS + '/' + id,
        data: JSON.stringify({score: value}),
        headers: {
            'x-auth-token': Cookies.get('token')
        }
    }).then(
        function (data) {
            jQuery(elem).parents('.work').find('.badge').text(value);
            updateWorks();
        },
        function () {
            alert('sdfgsd');
        }
    )
}

function getWorks() {
    return jQuery.ajax({
        method: 'get',
        url: ENDPOINTS.BASE + ENDPOINTS.WORKS,
        headers: {
            'x-auth-token': Cookies.get('token')
        }
    })
}

function authenticate() {
    var userName = jQuery('#login_page').find('#username').val();
    var password = jQuery('#login_page').find('#password').val();
    jQuery.ajax({
        method: 'POST',
        url: ENDPOINTS.BASE + ENDPOINTS.LOGIN,
        data: JSON.stringify({
            username: userName,
            password: password
        })
    }).then(
        function (data) {
            Cookies.set('token', data.token);
            showMessage('success', 'You are successfully logged in.', 1000).then(
                function () {
                    showWorksPage();
                }
            );
        },
        function (err) {
            showMessage('danger', err.responseJSON.message)
        }
    )
}

function checkLogin() {
    return Cookies.get('token');
}

function showMessage(type, message, timeout) {
    var dfd = jQuery.Deferred();
    timeout = timeout || 1500;
    var template = jQuery('<div class="alert alert-' + type + '"><strong>' + message + '</strong></div>').appendTo('body');
    setTimeout(function () {
        dfd.resolve();
        template.slideUp('slow')
    }, timeout);
    return dfd.promise();
}

function ajaxSetup() {
    jQuery.ajaxSetup({
        dataType: "json",
        contentType: "application/json"
    });
    $(document).ajaxError(function (data, error) {
        if (error.status === 403) {
            showMessage('danger', 'You are not authenticated.')
        }
    });
    $(document).ajaxComplete(function(event, data) {
        if(data.getResponseHeader('x-auth-token')){
            Cookies.set('token', data.getResponseHeader('x-auth-token'));
        }
    })
}

function logout() {
    Cookies.remove('token');
    showLoginPage();
}

function getWorkItem(id, name, score) {
    if (score === null) {
        score = '';
    }
    return jQuery('<a class="list-group-item work" data-id="' + id + '"> ' + name +

        '<div class="btn-group score-btn-group pull-right">' +
        '<button type="button" class="btn btn-danger" onclick="rate(this, 10)">10</button>' +
        '<button type="button" class="btn btn-success" onclick="rate(this, 0)">0</button>' +
        '</div>' +
        '<span class="badge">' + score + '</span> ' +
        '<span class="clearfix"></span>'+
        '</a>');
}