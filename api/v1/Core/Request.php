<?php
namespace Works\Core;



use Works\Core\Exceptions\WrongUrlException;

class Request
{
    private $controller;
    private $action;
    private $argument;
    public $user;

    public function __construct() {
        $this->extractSegments();
    }

    /**
     * Extract segments from url
     * @throws WrongUrlException
     */
    private function extractSegments() {
        if(!empty($_SERVER['QUERY_STRING'])){
            // remove empty values and reindex array
            $segments = array_values(array_filter(explode('/', $_SERVER['QUERY_STRING'])));
            if(!empty($segments[0])){
                $this->controller = $segments[0];
            }
            if(!empty($segments[1])){
                $this->argument = $segments[1];
            }
            if($this->controller === 'login'){
                $this->controller = 'users';
                $this->action = 'login';
            }
        }
        if(empty($this->controller)){
            throw new WrongUrlException();
        }
    }

    public function getController() {
        return $this->controller;
    }

    /**
     * todo should be moved to Router
     * @return string
     */
    public function getAction() {
        //login action
        if($this->action && $_SERVER['REQUEST_METHOD'] == 'POST'){
            return $this->action;
        }
        switch ($_SERVER['REQUEST_METHOD']){
            case 'POST': {
                return 'create'.ucfirst($this->controller);
            }
            case 'PUT': {
                return 'update'.ucfirst($this->controller);
                break;
            }
            case 'DELETE': {
                return 'delete'.ucfirst($this->controller);
                break;
            }
            default: {
                return 'get'.ucfirst($this->controller);
            }
        }
    }

    public function getArgument() {
        return $this->argument;
    }

    public function getPost() {
        if(!empty($_POST)){
            return $_POST;
        }
        return json_decode(file_get_contents('php://input'), true);
    }

    public function getHeaders() {
        return getallheaders();
    }
}