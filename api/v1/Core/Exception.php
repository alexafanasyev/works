<?php

namespace Works\Core;


use Throwable;

class Exception extends \Exception
{
    protected $statusCode;

    public function __construct($message = "", $code = 500, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
        $this->statusCode = $code;
    }

    public function getStatusCode() {
        return $this->statusCode;
    }
}