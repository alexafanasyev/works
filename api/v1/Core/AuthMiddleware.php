<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19.05.2017
 * Time: 17:59
 */

namespace Works\Core;


use Firebase\JWT\JWT;
use Works\Exceptions\UnauthenticatedException;

class AuthMiddleware
{
    public function __construct(Request $request, Response $response) {
        global $configs;
        if($request->getAction() != 'login'){
            $headers = $request->getHeaders();
            if(empty($headers['x-auth-token'])){
                throw new UnauthenticatedException();
            }
            $request->user = JWT::decode($headers['x-auth-token'], $configs['JWT_SECRET'], array('HS256'));
            $response->addHeader('x-auth-token', JWT::encode($request->user, $configs['JWT_SECRET']));
        }
    }
}