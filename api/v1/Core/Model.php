<?php
namespace Works\Core;


abstract class Model extends \PDO
{
    protected $table;

    public function __construct($table) {
        global $configs;
        $dsn = $configs['DB_DRIVER'].':dbname='.$configs['DB_NAME'].';host='.$configs['DB_HOST'];
        $username = $configs['DB_USER'];
        $passwd = $configs['DB_PASSWORD'];
        $this->table = $table;
        parent::__construct($dsn, $username, $passwd);
    }

    public function findAll($fields = array()) {
        $_fields = !empty($fields) ? join(',', $fields) : '*';
        $sth = $this->prepare("SELECT $_fields FROM $this->table");
        $sth->execute();
        if($sth->errorCode() !== \PDO::ERR_NONE){
            throw new \Exception('Database error: '.$sth->errorInfo()[2]);
        }
        $sth->setFetchMode(\PDO::FETCH_ASSOC);
        return $sth->fetchAll();
    }

    public function findById($id) {
        $sth = $this->prepare("SELECT * FROM $this->table WHERE id = :id");
        $sth->execute(array(':id'=>$id));
        if($sth->errorCode() !== \PDO::ERR_NONE){
            throw new \Exception('Database error: '.$sth->errorInfo()[2]);
        }
        $sth->setFetchMode(\PDO::FETCH_ASSOC);
        return $sth->fetchAll();
    }

    public function updateUserWorkScore($userId, $workId, $score) {
        $sth = $this->prepare("UPDATE $this->table SET score = :score WHERE id = :id and user_id = :userId");
        $sth->execute(array(
            ':id' => $workId,
            ':score' => $score,
            ':userId' => $userId
        ));
        if($sth->errorCode() !== \PDO::ERR_NONE){
            throw new \Exception('Database error: '.$sth->errorInfo()[2]);
        }
        $sth->setFetchMode(\PDO::FETCH_ASSOC);
        return $sth->fetchAll();
    }
}