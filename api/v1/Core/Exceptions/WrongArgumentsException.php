<?php

namespace Works\Core\Exceptions;


use Works\Core\Exception;

class WrongArgumentsException extends Exception
{
    function __construct($method='') {
        $message = "Wrong arguments supllied for '$method' method.";
        parent::__construct($message, 400);
    }
}