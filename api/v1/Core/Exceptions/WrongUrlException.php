<?php
namespace Works\Core\Exceptions;

use Works\Core\Exception;

class WrongUrlException extends Exception
{
    function __construct($message = "", $code = 500, $previous = null) {
        parent::__construct($message = "", $code = 500, $previous = null);
        $this->message = $message ? $message : 'Wrong url specified';
    }
}