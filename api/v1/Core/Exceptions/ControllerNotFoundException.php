<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19.05.2017
 * Time: 16:25
 */

namespace Works\Core\Exceptions;

use Works\Core\Exception;

class ControllerNotFoundException extends Exception
{
    function __construct($controllerName) {
        $message = "Controller '$controllerName' not found.";
        parent::__construct($message);
    }
}