<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19.05.2017
 * Time: 16:25
 */

namespace Works\Core\Exceptions;

use Works\Core\Exception;

class MethodNotFoundException extends Exception
{
    function __construct($method='') {
        $message = "Method '$method' not found.";
        parent::__construct($message);
    }
}