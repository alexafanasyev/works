<?php
namespace Works\Core;
abstract class Controller implements HooksInterface
{
    protected $response;

    protected $request;

    public function __construct(Request $request, Response $response) {
        $this->request = $request;
        $this->response = $response;
    }

    public function __destruct() {
    }
}