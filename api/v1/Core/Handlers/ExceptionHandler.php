<?php

namespace Works\Core\Handlers;


use Works\Core\Exception;
use Works\Core\Response;

class ExceptionHandler
{
    public static function worksExceptionHandler(\Throwable $e) {
        global $configs;
        $statusCode = 500;
        $message = $e->getMessage();

        if($e instanceof Exception){
            $statusCode = $e->getStatusCode();
        }

        $message = $message ? $message : $configs['defaultErrorMessage'];

        $response = new Response();
        $response->setStatusCode($statusCode);
        $response->setData(array(
            'success'=>false,
            'message'=>$message
        ));
        $response->send();
    }

    public static function attachHandlers() {
        set_exception_handler(array(self::class, 'worksExceptionHandler'));
    }
}