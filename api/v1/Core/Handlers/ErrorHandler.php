<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19.05.2017
 * Time: 15:20
 */

namespace Works\Core\Handlers;


use Works\Core\Response;

class ErrorHandler
{
    public static $fatalCodes = array(E_CORE_ERROR, E_COMPILE_ERROR, E_ERROR);

    /**
     * Function to handle fatal errors
     */
    public static function worksFatalHandler() {
        global $configs;
        $error = error_get_last();
        if ($error !== NULL && in_array($error['type'], self::$fatalCodes)) {
            $message = '';
            if (ENVIRONMENT == 'DEVELOPMENT') {
                $message = $error["type"] . ': ' . $error["message"] . ', at file ' . $error["file"] . ', line ' . $error["line"];
            }
            $message = $message ? $message : $configs['defaultErrorMessage'];
            $response = new Response();
            $response->setStatusCode(500);
            $response->setData(array(
                'success'=>false,
                'message'=>$message
            ));
            $response->send();
        }
    }

    public static function worksErrorHandler($code, $message) {
        // todo do some stuff with other errors
    }

    public static function attachHandlers() {
        // disable error reporting to avoid response destruction
        error_reporting(0);
        register_shutdown_function(array(self::class, 'worksFatalHandler'));
        set_error_handler(array(self::class, 'worksErrorHandler'));
    }
}