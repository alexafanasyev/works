<?php

namespace Works\Core;


interface HooksInterface
{
    public function beforeAction();
    public function beforeResponse();
}