<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19.05.2017
 * Time: 15:10
 */

namespace Works\Core;


class Response
{
    private $statusCode = 200;

    private $data = array();

    public $isSent = false;

    private $headers = array();

    /**
     * @param int $code
     */
    public function setStatusCode($code) {
        $this->statusCode = $code;
    }

    public function send() {
        http_response_code($this->statusCode);
        foreach ($this->headers as $headerName => $headerValue) {
            header("$headerName: $headerValue");
        }
        echo json_encode($this->data);
        $this->isSent = true;
        exit;
    }

    /**
     * @param array $data
     */
    public function setData($data=array()) {
        $this->data = $data;
    }

    public function addHeader($name, $value) {
        $this->headers[$name] = $value;
    }
}