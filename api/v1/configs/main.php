<?php
$configs['defaultErrorMessage'] = 'Unknown error';

// db credentials
//todo will be better to move these to .env file
$configs['DB_HOST'] = 'localhost';
$configs['DB_USER'] = 'root';
$configs['DB_DRIVER'] = 'mysql';
$configs['DB_PASSWORD'] = '';
$configs['DB_NAME'] = 'works_db';

//JWT configs
$configs['JWT_SECRET'] = 'some_LoNg-string^here';