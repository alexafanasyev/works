<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19.05.2017
 * Time: 18:10
 */

namespace Works\Exceptions;


use Throwable;
use Works\Core\Exception;

class TokenNotProvidedException extends Exception
{
    public function __construct() {
        parent::__construct('Auth token is not provided', 403);
    }
}