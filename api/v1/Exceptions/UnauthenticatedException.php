<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19.05.2017
 * Time: 18:06
 */

namespace Works\Exceptions;


use Works\Core\Exception;

class UnauthenticatedException extends Exception
{
    public function __construct() {
        parent::__construct('You are not authenticated', 403);
}
}