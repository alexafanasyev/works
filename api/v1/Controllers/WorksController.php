<?php
namespace Works\Controllers;

use Works\Core\Controller;
use Works\Core\Exceptions\WrongArgumentsException;
use Works\Core\Request;
use Works\Core\Response;
use Works\Models\WorksModel;

class WorksController extends Controller
{
    /**
     * @var WorksModel $model
     */
    private $model;

    public function beforeAction() {
        $this->model = new WorksModel();
    }

    public function getWorks() {
        $works = $this->model->findByUserId($this->request->user->id);
        $this->response->setData($works);
        $this->response->send();
    }

    public function updateWorks($id) {
        $score = $this->request->getPost()['score'];
        if(!isset($score)){
            throw new WrongArgumentsException('updateWorks');
        }
        $this->model->updateUserWorkScore($this->request->user->id, $id, $score);
        $this->response->send();
    }

    public function beforeResponse() {

    }
}