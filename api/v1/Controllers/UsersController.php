<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19.05.2017
 * Time: 18:36
 */

namespace Works\Controllers;


use Firebase\JWT\JWT;
use Works\Core\Controller;
use Works\Core\Exceptions\WrongArgumentsException;
use Works\Models\UsersModel;

class UsersController extends Controller
{
    /**
     * @var UsersModel $model
     */
    private $model;

    public function beforeAction() {
        $this->model =  new UsersModel();
    }

    public function login() {
        global $configs;
        $postData = $this->request->getPost();
        if(empty($postData['username']) || empty($postData['password'])){
            throw new WrongArgumentsException('login');
        }
        $pass = md5($postData['password']);
        $user = $this->model->findByUsernamePassword($postData['username'], $pass);
        if(empty($user)){
            $this->response->setStatusCode(403);
            $this->response->setData(array('success'=>false));
        }else{
            unset($user['password']);
            $this->response->setData(array('success'=>true, 'token'=> JWT::encode($user, $configs['JWT_SECRET'])));
        }
        $this->response->send();
    }

    public function beforeResponse() {

    }

}