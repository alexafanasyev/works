<?php

// define app global constants
if(!defined('DS')){
    define('DS', DIRECTORY_SEPARATOR);
}
define('BASE_DIR', __DIR__);

//define environment
if(getenv('ENVIRONMENT')){
    define('ENVIRONMENT', getenv('ENVIRONMENT'));
}else{
    define('ENVIRONMENT', 'PRODUCTION');
}

// check if composer dependencies are properly installed
if(!file_exists(BASE_DIR.DS.'vendor'.DS.'autoload.php')){
    exit('Please run "composer install".');
}
// load app configs
require (BASE_DIR.DS.'configs'.DS.'main.php');

// load composer autoloader
require(BASE_DIR.DS.'vendor'.DS.'autoload.php');

//define handlers
Works\Core\Handlers\ExceptionHandler::attachHandlers();
Works\Core\Handlers\ErrorHandler::attachHandlers();
$request = new Works\Core\Request();
$response = new \Works\Core\Response();

// authentication
$auth = new \Works\Core\AuthMiddleware($request, $response);

// routing
$controllerName = $request->getController();
$controllerClass = '\\Works\\Controllers\\'.ucfirst($controllerName).'Controller';
/**
 *@var $controller \Works\Core\Controller
 */
if(class_exists($controllerClass)){

    $controller = new $controllerClass($request, $response);
    $action = $request->getAction();
    if($request->getAction() && method_exists($controller, $action)){
        $controller->beforeAction();
        $controller->{$action}($request->getArgument());
        $controller->beforeResponse();
    }else{
        throw new \Works\Core\Exceptions\MethodNotFoundException($action);
    }
}else{
    throw new Works\Core\Exceptions\ControllerNotFoundException($controllerName);
}
