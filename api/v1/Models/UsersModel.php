<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19.05.2017
 * Time: 18:38
 */

namespace Works\Models;


use Works\Core\Model;

class UsersModel extends Model
{
    public function __construct() {
        parent::__construct('users');
    }

    public function findByUsernamePassword($username, $password) {
        $sth = $this->prepare("SELECT * FROM $this->table WHERE name = :username and password = :password");
        $sth->execute(array(
            ':username' => $username,
            ':password' => $password
            ));
        if ($sth->errorCode() !== \PDO::ERR_NONE) {
            throw new \Exception('Database error: ' . $sth->errorInfo()[2]);
        }
        $sth->setFetchMode(\PDO::FETCH_ASSOC);
        $user = $sth->fetchAll();
        if(!empty($user[0])){
            return $user[0];
        }
        return array();
    }
}