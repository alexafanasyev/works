<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19.05.2017
 * Time: 13:58
 */

namespace Works\Models;

use Works\Core\Model;

class WorksModel extends Model
{
    public function __construct() {
        parent::__construct('works');
    }

    public function findByUserId($userId) {
        $sth = $this->prepare("SELECT * FROM $this->table WHERE user_id = :id");
        $sth->execute(array(':id' => $userId));
        if ($sth->errorCode() !== \PDO::ERR_NONE) {
            throw new \Exception('Database error: ' . $sth->errorInfo()[2]);
        }
        $sth->setFetchMode(\PDO::FETCH_ASSOC);
        return $sth->fetchAll();
    }
}